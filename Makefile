REG_ADDRESS=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

php-fpm-build:
	docker-compose -f php-fpm.yml build

php-fpm-tag:
	docker tag php_fpm:latest ${REG_ADDRESS}/${USERNAME}/docker-php-fpm:base-${VERSION} && \
	docker tag ${REG_ADDRESS}/${USERNAME}/docker-php-fpm:base-${VERSION} ${REG_ADDRESS}/${USERNAME}/docker-php-fpm:base-latest

php-fpm-push:
	docker push ${REG_ADDRESS}/${USERNAME}/docker-php-fpm-images:base-${VERSION} && \
	docker push ${REG_ADDRESS}/${USERNAME}/docker-php-fpm:base-latest
